-- vim:filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:fdm=marker:foldmarker={{{,}}}
-- {{{ conky
run_once("conky")
-- }}}
--
-- {{{ Xscreensaver
run_once("xscreensaver -no-splash")
-- }}}
--
-- {{{ lightsOn.sh - prevent lock screen while watching fullscreen
run_once("lightsOn.sh")
-- }}}
--
-- {{{ Dvorak
run_once("setxkbmap dvorak")
-- }}}
--
-- {{{ Spotify
run_once("spotify")
run_once("blockify")
-- }}}
