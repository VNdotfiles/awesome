-- vim:filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:fdm=marker:foldmarker={{{,}}}
-- Rules to apply to new clients (through the "manage" signal).
-- My HD3000 graphic card only support 2 screen -- TODO
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     size_hints_honor = false,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    -- Set Firefox to always map on tags number 2 of last screen
    { rule = { class = "Firefox", instance = "Navigator" },
      properties = { tag = tags[screen.count()][2] } },
    -- Plugin of firefox must be floating window
    { rule = { instance = "plugin-container" },
      properties = { floating = true } },
    -- DownThemAll Manager must be floating window
    { rule = { instance = "DTA" },
      properties = { floating = true } },
    -- Shutter must be floating window
    { rule = { instance = "shutter" },
      properties = { floating = true } },
    -- Set Gvim to always map on tags number 3 of last screen
    { rule = { class = "Gvim" },
      properties = { tag = tags[screen.count()][3] } },
    -- Set smplayer to always map on tags number 4 of last screen
    { rule = { class = "smplayer" },
      properties = { tag = tags[screen.count()][4] } },
    -- Set Spotify to always map on tags number 4 of last screen
    { rule = { class = "Spotify" },
      properties = { tag = tags[screen.count()][4] } },
    -- ranger via urxvt must be floating window
    { rule = { class = "URxvt" },
      properties = { floating = true } },
    -- mpv must be floating window
    { rule = { class = "mpv" },
      properties = { floating = true } },
    -- Set Telegram to always map on tags number 5 of first screen
    { rule = { class = "TelegramDesktop", instance = "telegram" },
      properties = { tag = tags[1][5] } },
    -- Set Steam to always map on tags number 7 of last screen
    { rule = { class = "Steam" },
      properties = { tag = tags[screen.count()][7] } },
    -- Set Zim to always map on tags number 6 of last screen
    { rule = { class = "Zim" },
      properties = { tag = tags[screen.count()][6] } },
    -- Set Desktop of Gnome must be share all workspace
    { rule = { class = "Nautilus", instance = "desktop_window" },
      properties = { sticky = true } },
    -- Set File manager of Gnome to always map on tags number 8 of last screen
    { rule = { class = "Nautilus", instance = "nautilus" },
      properties = { tag = tags[screen.count()][8] } },
    -- Set Double Commander to always map on tags number 8 of last screen
    { rule = { class = "Doublecmd" },
      properties = { tag = tags[screen.count()][8] } },
    -- Set qbittorrent to always map on tags number 9 of last screen
    { rule = { class = "qBittorrent" },
      properties = { tag = tags[screen.count()][9] } },
    -- Set Zeal to always maps on tags number 6 of last screen
    { rule = { class = "Zeal" },
      properties = { tag = tags[screen.count()][6] } },
}
