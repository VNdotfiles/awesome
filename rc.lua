-- vim:filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:fdm=marker:foldmarker={{{,}}}
-- {{{ Standard awesome library
local gears             = require("gears")
awful                   = require("awful")
awful.rules             = require("awful.rules")
require("awful.autofocus")
-- Theme handling library
beautiful               = require("beautiful")
-- }}}

-- {{{ Other library
-- Redshift library
redshift                = require("redshift")
-- Error handling, variable definitions and function definitions
require("helpers")
require("dynamo")
-- }}}

-- {{{ Autostart before to save time
require_exist("before")
-- }}}

-- {{{ Themes define colours, icons, font and wallpapers.
beautiful.init(awful.util.getdir("config") .. "/themes/theme.lua")
-- }}}

for s = 1, screen.count() do
    gears.wallpaper.maximized("/home/vunhan/Pics/LinuxWallpaper/Lion.png", s, true)
end
-- }}}

-- {{{ Tags
for s = 1, screen.count() do
    tags[s] = awful.tag(tags.names, s, tags.layout)
end
-- }}}

-- {{{ Menu
require_exist("menu")
-- }}}

-- {{{ Init basic config
-- Mouse bindings
require("mouse")

-- Panel
require("panel")

-- Key bindings
require("keyboard")

-- Rules
require("rule")

-- Signals
require("signal")
-- }}}

-- {{{ Autostart after init all
require_exist("after")
-- }}}
