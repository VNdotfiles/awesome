-- vim:filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:fdm=marker:foldmarker={{{,}}}

--  {{{ Set background
-- run_once("feh --bg-scale ~/Pictures/wallpaper/wolf.jpg") 
--  }}}

-- TODO - lain - wifi

-- {{{ Hide mouse if not use
run_once("unclutter")
-- }}}

-- {{{ Web Browser
run_once(browser)
-- }}}

-- {{{ Clipboard
run_once("clipit")
-- }}}

-- {{{ telegram
run_once("telegram")
-- }}}

-- {{{ Terminal
run_once(terminal, "-e 'zsh -c tmux'")
-- }}}

-- {{{ Mpd
run_once("mpd")
-- }}}

-- {{{ Composite manager
run_once("compton")
-- }}}

-- {{{ fcitx
run_once("fcitx")
-- }}}

-- {{{ redshift
run_once("redshift")
-- }}}
